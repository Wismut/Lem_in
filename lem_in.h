/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/31 18:00:56 by mivanov           #+#    #+#             */
/*   Updated: 2017/06/12 21:05:51 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H
# include "libft/libft.h"
# include "libft/get_next_line.h"

typedef struct		s_link
{
	char			*link1;
	char			*link2;
	struct s_link	*next;
}					t_link;

typedef struct		s_room
{
	char			*name;
	int				x;
	int				y;
	struct s_room	*next;
}					t_room;

typedef struct		s_general
{
	int				count_ants;
	t_room			*rooms;
	t_link			*links;
	t_room			*start;
	t_room			*end;
	t_room			*way;
}					t_general;

void				add_start(t_general **general, char *str, int fd);
void				add_lines(int fd, t_general *general, int i);
int					is_room(char *str);
int					is_link(char *str);
int					is_comment(char *str);
int					count_array(char **arr);
void				ft_error(char *str);
void				add_room(t_room **rooms, char *str);
int					is_room_valid(t_room *rooms, char *str, t_general *general);
int					is_start(char *str);
int					is_end(char *str);
void				add_link(t_link **links, char *str);
int					is_room_present(t_general *general, t_room *rooms,
char *str);
int					is_link_valid(t_general *general, t_room *rooms, char *str);
t_room				*get_start(char *str);
t_room				*get_end(char *str);
void				add_end(t_general **general, char *str, int fd);
int					is_int(char *str);
int					count_rooms(t_room *rooms);
int					is_rooms_connected(t_link *links, t_room *room1,
t_room *room2);
int					is_room_visited(t_room *way, t_room *room);
void				search(t_general *general, t_room *way);
void				add_room_to_way(t_room *way, t_room *room);
void				print_answer(t_general *general, t_room *way);
int					ft_free_split(char **array);
void				free_rooms(t_room **rooms);
void				free_general(t_general **general);
void				free_links(t_link **links);
t_room				*get_room_to_general(char *str, t_room **rooms,
t_general *general);
void				add_link_general(char *str, t_room *rooms,
t_general *general);
t_general			*add_end_general(int fd, char *str, t_general **general);
t_general			*get_general(int fd, char *str, t_general **general);
void				add_count(char *str, t_general *general);
t_room				*get_new_way(t_general *general);
t_room				*get_new_rooms(void);
t_link				*get_new_links(void);
t_general			*get_new_general(void);
void				free_and_exit(char *str);
int					check_way(t_room *way, t_general *general);
int					is_present(char *name, t_general general);
int					del_after_end(t_room *way, t_general *general);
t_room				**search_short_way(t_room *start, t_room *rooms, t_link *links, t_room *end);
void				work_with_links(t_room **all_links, t_general *general);

#endif
