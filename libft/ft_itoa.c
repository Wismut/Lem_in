/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 19:13:49 by mivanov           #+#    #+#             */
/*   Updated: 2016/12/01 18:54:27 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	int				i;
	unsigned int	number;
	char			*str;

	i = 0;
	number = n < 0 ? -n : n;
	while (number > 0 && ++i)
		number /= 10;
	if (n <= 0)
		i++;
	if (!(str = (char*)malloc((i + 1) * sizeof(*str))))
		return (0);
	str[i--] = '\0';
	number = n < 0 ? -n : n;
	while (i >= 0)
	{
		str[i--] = number % 10 + 48;
		number /= 10;
	}
	if (n < 0)
		str[0] = '-';
	return (str);
}
