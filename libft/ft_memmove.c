/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 20:47:20 by mivanov           #+#    #+#             */
/*   Updated: 2016/11/29 14:17:08 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char	*newdst;
	char	*newsrc;

	newdst = (char *)dst;
	newsrc = (char *)src;
	if (newdst != newsrc)
	{
		if (newdst > newsrc)
		{
			while (len-- > 0)
				newdst[len] = newsrc[len];
		}
		else
			while (len-- > 0)
				*newdst++ = *newsrc++;
	}
	return (dst);
}
