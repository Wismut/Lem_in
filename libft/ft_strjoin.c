/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 15:18:50 by mivanov           #+#    #+#             */
/*   Updated: 2016/11/28 17:04:43 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*str;

	if (!s1 || !s2)
		return (NULL);
	str = (char *)ft_memalloc(ft_strlen(s1) + ft_strlen(s2) + 1);
	if (str)
	{
		ft_strcat(str, s1);
		ft_strcat(str, s2);
	}
	return (str);
}
