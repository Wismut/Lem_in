/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 19:50:38 by mivanov           #+#    #+#             */
/*   Updated: 2016/12/01 19:23:12 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_len_char(char const *s, char c)
{
	char *p;

	if (!*s)
		return (0);
	while (*s == c && *s)
		s++;
	p = (char *)s;
	while (*p != c && *p)
		p++;
	return (p - s);
}

int		ft_count_words(char const *s, char c)
{
	char	*p;
	int		cw;

	cw = 0;
	if (!*s)
		return (0);
	while (*s)
	{
		while (*s == c && *s)
			s++;
		p = (char *)s;
		while (*p != c && *p)
			p++;
		(*s != c && *s) ? cw++ : 0;
		s = p;
	}
	return (cw);
}

char	**ft_strsplit(char const *s, char c)
{
	char	**res;
	char	*p1;
	char	**p2;

	if (!s || !(res = malloc(sizeof(char*) * (ft_count_words(s, c) + 1))))
		return (NULL);
	p2 = res;
	while (*s)
	{
		while (*s == c && *s && s++)
			;
		if (*s)
		{
			if (!(p1 = malloc(ft_len_char(s, c) + 1)))
				return (NULL);
			*res = p1;
			while (*s != c && *s && (*p1++ = *s++))
				;
			*p1 = '\0';
			res++;
		}
	}
	*res = NULL;
	return (p2);
}
