/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 14:00:58 by mivanov           #+#    #+#             */
/*   Updated: 2016/11/29 20:30:15 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *big, const char *little)
{
	char	*newb;
	char	*newl;

	if (*little == 0)
		return ((char*)big);
	while (*big)
	{
		newb = (char*)big;
		newl = (char*)little;
		while (*newl == *newb && *newl)
		{
			newl++;
			newb++;
		}
		if (*newl == 0)
			return ((char*)big);
		big++;
	}
	return (0);
}
