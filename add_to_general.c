/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_to_general.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/31 17:58:49 by mivanov           #+#    #+#             */
/*   Updated: 2017/06/07 16:59:23 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		add_count(char *str, t_general *general)
{
	if (!general->count_ants)
		general->count_ants = ft_atoi(str);
	else
		ft_error("ERROR\n");
	if (ft_atoi(str) == 0)
		ft_error("ERROR\n");
}

t_general	*get_general(int fd, char *str, t_general **general)
{
	add_start(general, str, fd);
	return (*general);
}

t_general	*add_end_general(int fd, char *str, t_general **general)
{
	add_end(general, str, fd);
	return (*general);
}

void		add_link_general(char *str, t_room *rooms, t_general *general)
{
	add_link(&general->links, str);
	!is_link_valid(general, rooms, str) ? ft_error("ERROR\n") : 0;
}

t_room		*get_room_to_general(char *str, t_room **rooms, t_general *general)
{
	add_room(rooms, str);
	!is_room_valid((*rooms), str, general) ? ft_error("ERROR\n") : 0;
	return (*rooms);
}
