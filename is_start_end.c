/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_start_end.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/31 18:00:33 by mivanov           #+#    #+#             */
/*   Updated: 2017/05/31 18:00:34 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		is_start(char *str)
{
	if (!str)
		return (0);
	return (!ft_strcmp(str, "##start") ? 1 : 0);
}

int		is_end(char *str)
{
	if (!str)
		return (0);
	return (!ft_strcmp(str, "##end") ? 1 : 0);
}
