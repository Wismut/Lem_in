/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_answer.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/31 18:01:34 by mivanov           #+#    #+#             */
/*   Updated: 2017/06/06 21:02:36 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	print_step(t_room *way, long *ants, long count)
{
	while (way && way->name)
	{
		if (ants[count] != 0)
		{
			ft_putchar('L');
			ft_putnbr(ants[count]);
			ft_putchar('-');
			ft_putstr(way->name);
			ft_putchar(' ');
		}
		count++;
		way = way->next;
	}
	ft_putstr("\n");
}

void	print_answer(t_general *general, t_room *way)
{
	long	len;
	long	*ants;
	long	i;
	long	j;
	long	count;

	i = -1;
	j = general->count_ants;
	len = count_rooms(way);
	count = (long)general->count_ants + len - 2;
	ants = (long *)malloc(sizeof(long) * (2 * len +
(long)general->count_ants - 1));
	while (++i < len - 1)
		ants[i] = 0;
	while (j-- > 0)
		ants[i++] = j + 1;
	i--;
	while (++i < general->count_ants + 2 * len)
		ants[i] = 0;
	while (--general->count_ants > -len)
	{
		print_step(way, ants, count);
		count--;
	}
	free(ants);
}
