/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_string.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/31 17:59:03 by mivanov           #+#    #+#             */
/*   Updated: 2017/06/12 21:04:04 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		is_int(char *str)
{
	int		i;
	long	value;

	i = -1;
	if (!str)
		return (0);
	if (!str[0])
		return (0);
	if (ft_strlen(str) > 11)
		return (0);
	value = ft_atol(str);
	value < -2147483648 || value > 2147483647 ? ft_error("Error\n") : 0;
	while (str[++i])
		if (str[i] < '0' || str[i] > '9')
			return (0);
	return (1);
}

int		is_room(char *str)
{
	char	**array;

	if (!str)
		return (0);
	if (!str[0] || str[0] == 'L')
		return (0);
	array = ft_strsplit(str, ' ');
	if (count_array(array) != 3)
		return (ft_free_split(array));
	if (!is_int(array[1]) || !is_int(array[2]))
		return (ft_free_split(array));
	if (ft_strlen(array[0]) + ft_strlen(array[1]) + ft_strlen(array[2]) !=
			ft_strlen(str) - 2)
		return (ft_free_split(array));
	return (ft_free_split(array) + 1);
}

int		is_link(char *str)
{
	char	**array;

	if (!str)
		return (0);
	if (!str[0])
		return (0);
	array = ft_strsplit(str, '-');
	if (count_array(array) != 2)
		return (ft_free_split(array));
	if (ft_strlen(array[0]) + ft_strlen(array[1]) + 1 != ft_strlen(str))
		return (ft_free_split(array));
	return (ft_free_split(array) + 1);
}

int		is_comment(char *str)
{
	if (!str)
		return (0);
	if (!str[0])
		return (0);
	if (str[0] == '#' && ft_strcmp(str, "##start") && ft_strcmp(str, "##end"))
		return (1);
	return (0);
}

int		check_way(t_room *way, t_general *general)
{
	while (way->next)
	{
		if (!ft_strcmp(way->name, general->end->name))
			break ;
		way = way->next;
	}
	if (ft_strcmp(way->name, general->end->name))
		return (0);
	return (1);
}
