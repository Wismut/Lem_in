/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_linked_list.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/31 18:00:19 by mivanov           #+#    #+#             */
/*   Updated: 2017/05/31 18:00:20 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	free_rooms(t_room **rooms)
{
	t_room	*tmp;

	if (!rooms)
	{
		free(rooms);
		return ;
	}
	while (*rooms != NULL)
	{
		tmp = (*rooms)->next;
		free((*rooms)->name);
		(*rooms)->next = NULL;
		free(*rooms);
		*rooms = tmp;
	}
}

void	free_links(t_link **links)
{
	t_link	*tmp;

	if (!links)
	{
		free(links);
		return ;
	}
	while (*links != NULL)
	{
		tmp = (*links)->next;
		*(*links)->link1 != '\0' ? free((*links)->link1) : 0;
		*(*links)->link2 != '\0' ? free((*links)->link2) : 0;
		(*links)->next = NULL;
		free(*links);
		*links = tmp;
	}
}

void	free_general(t_general **general)
{
	if (!general)
	{
		free(general);
		return ;
	}
	free_rooms(&(*general)->start);
}

int		ft_free_split(char **array)
{
	char	**tmp;

	if (!array || !*array)
		return (0);
	tmp = array;
	while (*array)
		free(*array++);
	free(tmp);
	return (0);
}
