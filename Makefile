#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mivanov <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/05 15:25:00 by mivanov           #+#    #+#              #
#    Updated: 2017/05/31 18:58:54 by mivanov          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = lem-in

G = gcc

FLAG = -Wall -Wextra -Werror

LIB = libft.a

SRC = main.c read_data.c valid.c search_way.c room.c is_start_end.c links.c print_answer.c free_linked_list.c counts.c add_start_end.c check_string.c add_to_general.c search_short_way.c

OBJ = $(SRC:.c=.o)

LIBFTDIR = libft

all: $(NAME)

$(NAME) : $(OBJ) $(LIB)
			@$(G) $(FLAG) $(OBJ) -o $(NAME) libft/$(LIB) -o $(NAME)
%.o: %.c
			@$(G) $(FLAG) -c $<

$(LIB) :
		@make -C libft/

cleanlib:
		@make -C libft/ clean

fcleanlib:
		@make -C libft/ fclean

clean: cleanlib
			@rm -f $(OBJ)

fclean: clean fcleanlib
			@rm -f $(NAME)

re: fclean all