/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_start_end.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/31 17:58:25 by mivanov           #+#    #+#             */
/*   Updated: 2017/06/07 18:08:34 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	add_start(t_general **general, char *str, int fd)
{
	get_next_line(fd, &str);
	ft_putstr(str);
	ft_putchar('\n');
	if (!is_room(str) || !(*general)->count_ants)
		ft_error("ERROR\n");
	if (is_present(str, **general))
		ft_error("ERROR\n");
	(*general)->start = get_start(str);
	free(str);
}

void	add_end(t_general **general, char *str, int fd)
{
	get_next_line(fd, &str);
	ft_putstr(str);
	ft_putchar('\n');
	if (!is_room(str) || !(*general)->count_ants)
		ft_error("ERROR\n");
	if (is_present(str, **general))
		ft_error("ERROR\n");
	(*general)->end = get_end(str);
	free(str);
}

t_room	*get_start(char *str)
{
	char	**array;
	t_room	*room;

	array = ft_strsplit(str, ' ');
	room = (t_room *)malloc(sizeof(t_room));
	room->name = ft_strdup(array[0]);
	room->x = ft_atoi(array[1]);
	room->y = ft_atoi(array[2]);
	room->next = NULL;
	ft_free_split(array);
	return (room);
}

t_room	*get_end(char *str)
{
	char	**array;
	t_room	*room;

	array = ft_strsplit(str, ' ');
	room = (t_room *)malloc(sizeof(t_room));
	room->name = ft_strdup(array[0]);
	room->x = ft_atoi(array[1]);
	room->y = ft_atoi(array[2]);
	room->next = NULL;
	ft_free_split(array);
	return (room);
}

void	free_and_exit(char *str)
{
	free(str);
	ft_error("ERROR\n");
}
