/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_data.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/31 18:01:48 by mivanov           #+#    #+#             */
/*   Updated: 2017/06/12 20:55:44 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	initialization(int *is_empty, t_link **links, t_room **rooms,
					t_general **general)
{
	*rooms = get_new_rooms();
	*links = get_new_links();
	*general = get_new_general();
	is_empty[0] = 0;
	is_empty[1] = 0;
	is_empty[2] = 0;
	is_empty[3] = 0;
	(*general)->rooms = *rooms;
	(*general)->links = *links;
	(*general)->count_ants = 0;
}

void	add_last_room(t_room *rooms, t_general *general)
{
	while (rooms->next)
		rooms = rooms->next;
	rooms->next = (t_room *)malloc(sizeof(t_room));
	rooms = rooms->next;
	rooms->name = general->end->name;
	rooms->x = general->end->x;
	rooms->y = general->end->y;
	rooms->next = NULL;
}

void	free_all(t_general **general, t_room **rooms)
{
	free_rooms(rooms);
	free_general(general);
	free_links(&(*general)->links);
}

void	solve_lemin(t_general **general, t_room **rooms)
{
	t_room	**all_links;

	ft_putchar('\n');
	(*general)->way = get_new_way((*general));
	add_last_room((*rooms), (*general));
	search((*general), (*general)->way);
	if (!del_after_end((*general)->way, *general) || !check_way((*general)->way, *general))
	{
		all_links = search_short_way((*general)->start, *rooms, (*general)->links, (*general)->end);
		if (!all_links)
			ft_error("ERROR\n");
		work_with_links(all_links, *general);
	}
	print_answer((*general), (*general)->way->next);
	free_all(general, rooms);
}

void	add_lines(int fd, t_general *general, int i)
{
	char	*str;
	t_room	*rooms;
	int		is_empty[4];
	t_link	*links;

	initialization(is_empty, &links, &rooms, &general);
	while (get_next_line(fd, &str) > 0 && ++i)
	{
		ft_putendl(str);
		if (is_int(str) && ++is_empty[0] && !is_empty[1] && is_empty[0] == 1)
			add_count(str, general);
		else if (is_link(str))
			add_link_general(str, rooms, general);
		else if (is_room(str) && general->count_ants && !links->link1)
			get_room_to_general(str, &rooms, general);
		else if (is_start(str) && ++is_empty[2] && is_empty[2] == 1)
			general = get_general(fd, str, &general);
		else if (is_end(str) && ++is_empty[3] && is_empty[3] == 1)
			general = add_end_general(fd, str, &general);
		else if (!is_comment(str))
			free_and_exit(str);
		free(str);
	}
	i ? free(str) : 0;
	solve_lemin(&general, &rooms);
}
