#include "lem_in.h"

t_room	**search_short_way(t_room *start, t_room *rooms, t_link *links, t_room *end)
{
	t_room	*cur;
	int 	i;
	int 	j;
	t_room	**all_links;
	t_room	*temp_room;

	i = -1;
	j = 0;
	cur = rooms;
	all_links = (t_room **)malloc(sizeof(t_room *) * 10000000);
	all_links[++i] = start;
	temp_room = cur;
	while (1)
	{
		cur = temp_room;
		while (cur)
		{
			if (i == 10000000)
				return (NULL);
			if (is_rooms_linked(links, cur, all_links[j]))
				all_links[++i] = cur;
			else
				all_links[++i] = NULL;
			if (is_rooms_linked(links, cur, all_links[j]) && !ft_strcmp(end->name, cur->name))
				return (all_links);
			cur = cur->next;
		}
		j++;
	}
}

void	work_with_links(t_room **all_links, t_general *general)
{
	int 	i;
	int 	j;
	t_room	**way;

	j = 0;
	way = (t_room **)malloc(sizeof(t_room *) * count_rooms(general->rooms));
	i = 0;
	while (i < 10000000)
	{
		if (!all_links[i])
		{
			i++;
			continue ;
		}
		if (!ft_strcmp(all_links[i]->name, general->end->name))
			break ;
		i++;
	}
	i++;
	if (i == 10000000)
		ft_error("ERROR\n");
	while (i > 0)
	{
		way[j] = all_links[i - 1];
		i /= count_rooms(general->rooms);
		j++;
	}
}
