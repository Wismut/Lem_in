/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/31 18:02:38 by mivanov           #+#    #+#             */
/*   Updated: 2017/06/07 18:10:20 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		ft_error(char *str)
{
	ft_putstr(str);
	exit(0);
}

t_room		*get_new_way(t_general *general)
{
	t_room	*way;

	way = (t_room *)malloc(sizeof(t_room));
	if (!general->start || !general->end)
		ft_error("ERROR\n");
	way->name = general->start->name;
	way->x = general->start->x;
	way->y = general->start->y;
	way->next = NULL;
	return (way);
}

t_room		*get_new_rooms(void)
{
	t_room *rooms;

	rooms = (t_room *)malloc(sizeof(t_room));
	rooms->next = NULL;
	rooms->name = NULL;
	return (rooms);
}

t_link		*get_new_links(void)
{
	t_link *links;

	links = (t_link *)malloc(sizeof(t_link));
	links->link1 = NULL;
	links->link2 = NULL;
	return (links);
}

t_general	*get_new_general(void)
{
	t_general *general;

	general = (t_general *)malloc(sizeof(t_general));
	general->start = NULL;
	general->end = NULL;
	return (general);
}
