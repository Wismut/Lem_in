/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   links.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/31 18:01:10 by mivanov           #+#    #+#             */
/*   Updated: 2017/06/07 17:59:05 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	add_link(t_link **links, char *str)
{
	t_link	*head;
	char	**array;

	array = ft_strsplit(str, '-');
	head = *links;
	if (!(*links)->link1)
	{
		*links = (t_link *)malloc(sizeof(t_link));
		(*links)->link1 = ft_strdup(array[0]);
		(*links)->link2 = ft_strdup(array[1]);
		(*links)->next = NULL;
		ft_free_split(array);
		free(head);
		return ;
	}
	while ((*links)->next)
		*links = (*links)->next;
	(*links)->next = (t_link *)malloc(sizeof(t_link));
	*links = (*links)->next;
	(*links)->link1 = ft_strdup(array[0]);
	(*links)->link2 = ft_strdup(array[1]);
	(*links)->next = NULL;
	*links = head;
	ft_free_split(array);
}

int		is_link_valid(t_general *general, t_room *rooms, char *str)
{
	char	**array;

	array = ft_strsplit(str, '-');
	if (!ft_strcmp(array[0], array[1]))
		return (ft_free_split(array));
	if (is_room_present(general, rooms, array[0]) &&
			is_room_present(general, rooms, array[1]))
		return (ft_free_split(array) + 1);
	return (ft_free_split(array));
}

int		is_rooms_connected(t_link *links, t_room *room1, t_room *room2)
{
	while (room1 && room1->next)
		room1 = room1->next;
	while (links && links->link1 && room1->name && room2->name)
	{
		if ((!ft_strcmp(links->link1, room1->name) &&
				!ft_strcmp(links->link2, room2->name)) ||
			(!ft_strcmp(links->link2, room1->name) &&
					!ft_strcmp(links->link1, room2->name)))
			return (1);
		links = links->next;
	}
	return (0);
}
