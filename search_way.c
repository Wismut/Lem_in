/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_way.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/31 18:02:28 by mivanov           #+#    #+#             */
/*   Updated: 2017/06/12 21:02:13 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	search(t_general *general, t_room *way)
{
	t_room	*head_rooms;

	head_rooms = general->rooms;
	while (1)
	{
		if (general->rooms &&
				is_rooms_connected(general->links, way, general->rooms) &&
			!is_room_visited(way, general->rooms))
		{
			add_room_to_way(way, general->rooms);
			search(general, way);
		}
		if (general->rooms)
			general->rooms = general->rooms->next;
		else
			break ;
	}
	general->rooms = head_rooms;
}

int		is_room_visited(t_room *way, t_room *room)
{
	while (way)
	{
		if (!ft_strcmp(way->name, room->name))
			return (1);
		way = way->next;
	}
	return (0);
}

void	add_room_to_way(t_room *way, t_room *room)
{
	if (!way)
		return ;
	while (way->next)
		way = way->next;
	way->next = (t_room *)malloc(sizeof(t_room));
	way = way->next;
	way->name = room->name;
	way->x = room->x;
	way->y = room->y;
	way->next = NULL;
}

int		del_after_end(t_room *way, t_general *general)
{
	while (way)
	{
		if (!ft_strcmp(general->end->name, way->name))
			break ;
		way = way->next;
	}
	if (!way)
		return (0);
	if (way && way->next)
		way->next->name = NULL;
	return (1);
}
