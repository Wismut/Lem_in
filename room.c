/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   room.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/31 18:01:59 by mivanov           #+#    #+#             */
/*   Updated: 2017/06/07 17:09:18 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	add_room(t_room **rooms, char *str)
{
	t_room	*head;
	char	**array;

	array = ft_strsplit(str, ' ');
	head = *rooms;
	if (!(*rooms)->name)
	{
		(*rooms)->name = ft_strdup(array[0]);
		(*rooms)->x = ft_atoi(array[1]);
		(*rooms)->y = ft_atoi(array[2]);
		(*rooms)->next = NULL;
		*rooms = head;
		ft_free_split(array);
		return ;
	}
	while ((*rooms)->next)
		*rooms = (*rooms)->next;
	(*rooms)->next = (t_room *)malloc(sizeof(t_room));
	*rooms = (*rooms)->next;
	(*rooms)->name = ft_strdup(array[0]);
	(*rooms)->x = ft_atoi(array[1]);
	(*rooms)->y = ft_atoi(array[2]);
	(*rooms)->next = NULL;
	ft_free_split(array);
	*rooms = head;
}

int		is_room_valid(t_room *rooms, char *str, t_general *general)
{
	char	**array;

	array = ft_strsplit(str, ' ');
	if ((general->start && !ft_strcmp(general->start->name, array[0]))
		|| (general->end && !ft_strcmp(general->end->name, array[0])))
		ft_error("ERROR\n");
	while (rooms->next)
	{
		if (!ft_strcmp(array[0], rooms->name))
			return (ft_free_split(array));
		if (rooms->x == ft_atoi(array[1]) && rooms->y == ft_atoi(array[2]))
			return (ft_free_split(array));
		rooms = rooms->next;
	}
	return (ft_free_split(array) + 1);
}

int		is_room_present(t_general *general, t_room *rooms, char *str)
{
	if ((general->start && !ft_strcmp(general->start->name, str)) ||
		(general->end && !ft_strcmp(general->end->name, str)))
		return (1);
	if (!rooms->name)
		return (0);
	while (rooms)
	{
		if (!ft_strcmp(str, rooms->name))
			return (1);
		rooms = rooms->next;
	}
	return (0);
}

int		count_rooms(t_room *rooms)
{
	int		i;

	i = 0;
	while (rooms && rooms->name)
	{
		i++;
		rooms = rooms->next;
	}
	return (i);
}

int		is_present(char *name, t_general general)
{
	char	**array;

	array = ft_strsplit(name, ' ');
	while (general.rooms)
	{
		if (general.rooms && general.rooms->name &&
!ft_strcmp(array[0], general.rooms->name))
			return (ft_free_split(array) + 1);
		general.rooms = general.rooms->next;
	}
	return (ft_free_split(array));
}
